package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.UserInfo;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.R;

import java.util.ArrayList;

public class NotepadAdapter extends BaseAdapter {

    private ArrayList<UserInfo> m_arrData = null;

    public NotepadAdapter(ArrayList<UserInfo> arrData){
        m_arrData = arrData;
    }


    @Override
    public int getCount() {
        return m_arrData.size();
    }

    @Override
    public Object getItem(int position) {
        return m_arrData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ((UserInfo)getItem(position)).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            LayoutInflater li = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.custom_view_coments, parent, false);
        }
        TextView fullNameTextView = (TextView) convertView.findViewById(R.id.textFullName);
        TextView phoneTextView = (TextView) convertView.findViewById(R.id.textPhone);
        TextView addressTextView = (TextView) convertView.findViewById(R.id.textAddress);

        UserInfo info = (UserInfo) getItem(position);

        fullNameTextView.setText(info.getFullName());
        phoneTextView.setText(info.getPhone());
        addressTextView.setText(info.getAddress());
        return convertView;
    }
}
