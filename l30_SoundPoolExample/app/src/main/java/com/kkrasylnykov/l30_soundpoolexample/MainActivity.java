package com.kkrasylnykov.l30_soundpoolexample;

import android.media.AudioManager;
import android.media.SoundPool;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private static final int MAX_STREAMS = 50;

    private SoundPool m_soundPool = null;

    private int nIdResourse = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_soundPool = new SoundPool(MAX_STREAMS, AudioManager.STREAM_MUSIC, 0);
        m_soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int i, int i1) {

            }
        });

        try {
            nIdResourse = m_soundPool.load(getAssets().openFd("shot.ogg"), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Button btnPlay = (Button) findViewById(R.id.btnPlay);
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m_soundPool.play(nIdResourse, 1, 1, 0, 0, 1);
            }
        });
    }
}
