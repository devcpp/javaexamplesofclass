package com.kkrasylnykov.l12_fragmentexample.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kkrasylnykov.l12_fragmentexample.R;

public class SecondFragment extends Fragment {

    private String m_strText = "";
    private TextView m_TextView = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmet_second,null);
        m_TextView = (TextView) view.findViewById(R.id.TV1F2);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (m_TextView!=null){
            m_TextView.setText(m_strText);
        }
    }

    public void setText(String strText){
        m_strText = strText;
        if (m_TextView!=null){
            m_TextView.setText(m_strText);
        }
    }
}
