package com.kkrasylnykov.l12_fragmentexample.Activites;

import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.kkrasylnykov.l12_fragmentexample.Fragments.FirstFragment;
import com.kkrasylnykov.l12_fragmentexample.R;
import com.kkrasylnykov.l12_fragmentexample.Fragments.SecondFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    FirstFragment m_firstFragment = null;
    SecondFragment m_secondFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*View btnAddFirstFragment = findViewById(R.id.addFirstFragmentButton);
        btnAddFirstFragment.setOnClickListener(this);
        View btnAddSecondFragment = findViewById(R.id.addSecondFragmentButton);
        btnAddSecondFragment.setOnClickListener(this);
        View btnRemoveFirstFragment = findViewById(R.id.removeFirstFragmentButton);
        btnRemoveFirstFragment.setOnClickListener(this);
        View btnRemoveSecondFragment = findViewById(R.id.removeSecondFragmentButton);
        btnRemoveSecondFragment.setOnClickListener(this);*/

        m_firstFragment = new FirstFragment();
        m_secondFragment = new SecondFragment();

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.FrameLayout1,m_firstFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View v) {
    }

    public void showSecondFragment(String strText){
        int idFL = R.id.FrameLayout1;
        if (getResources().getConfiguration().orientation== Configuration.ORIENTATION_LANDSCAPE){
            idFL = R.id.FrameLayout2;
        }
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(idFL,m_secondFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        m_secondFragment.setText(strText);
    }
}
