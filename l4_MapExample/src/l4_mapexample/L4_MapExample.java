
package l4_mapexample;

import java.util.HashMap;

public class L4_MapExample {


    public static void main(String[] args) {
        String strKey1 = "strKey1";
        String strKey2 = "strKey2";
        String strKey3 = "strKey3";
        String strKey4 = "strKey4";
        
        HashMap<String, Integer> arrMap = new HashMap<>();
        
        
        arrMap.put(strKey1, 10);
        arrMap.put(strKey2, 100);
        arrMap.put(strKey3, 1000);
        
        if (arrMap.containsKey(strKey4)){
            System.out.println("strKey4 is contain");
        } else {
            System.out.println("strKey4 isn't contain");
        }
        
        System.out.println("strKey2 -> " + arrMap.get(strKey2));
        
        String[] arrKeys = new String[arrMap.keySet().size()];
        arrMap.keySet().toArray(arrKeys);
        
        for (String skrKey:arrKeys){
            System.out.println("skrKey-> " + skrKey);
        }
        
        
    }
    
}
