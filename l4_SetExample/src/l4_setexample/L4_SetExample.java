package l4_setexample;

import java.util.HashSet;

public class L4_SetExample {

    public static void main(String[] args) {
        HashSet<Integer> arrSetExample = new HashSet<>();
        
        arrSetExample.add(23);
        arrSetExample.add(new Integer(15));
        int nVal = 10;
        arrSetExample.add(nVal);
        arrSetExample.add(15);
        
        Integer[] arr =  new Integer[arrSetExample.size()];
        
        arrSetExample.toArray(arr);
        
        for (int i=0;i<arr.length;i++){
            System.err.println(arr[i]);
        }
    }
    
}
