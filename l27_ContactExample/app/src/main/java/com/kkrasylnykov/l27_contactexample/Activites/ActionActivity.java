package com.kkrasylnykov.l27_contactexample.Activites;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.kkrasylnykov.l27_contactexample.R;

public class ActionActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String KEY_NAME = "KEY_NAME";
    public static final String KEY_PHONE = "KEY_PHONE";
    public static final String KEY_EMAIL = "KEY_EMAIL";

    private String m_strName = "";
    private String m_strPhone = "";
    private String m_strEmail = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actions);

        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                m_strName = bundle.getString(KEY_NAME,"");
                m_strPhone = bundle.getString(KEY_PHONE,"");
                m_strEmail = bundle.getString(KEY_EMAIL,"");
            }
        }

        TextView textViewName = (TextView) findViewById(R.id.textViewNameActionActivity);
        TextView textViewPhone = (TextView) findViewById(R.id.textViewPhoneActionActivity);
        TextView textViewEmail = (TextView) findViewById(R.id.textViewEmailActionActivity);

        textViewName.setText(m_strName);
        textViewPhone.setText(m_strPhone);
        textViewEmail.setText(m_strEmail);

        Button buttonSearch = (Button) findViewById(R.id.buttonSearchInfoActionActivity);
        Button buttonCall = (Button) findViewById(R.id.buttonCallActionActivity);
        Button buttonSend = (Button) findViewById(R.id.buttonSendEmailActionActivity);

        buttonSearch.setOnClickListener(this);
        buttonCall.setOnClickListener(this);
        buttonSend.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonSearchInfoActionActivity:
                String strUrl = "http://google.com/#q="+m_strName;

                Intent searchIntent = new Intent();
                searchIntent.setAction(Intent.ACTION_VIEW);
                searchIntent.setData(Uri.parse(strUrl));
                startActivity(searchIntent);

                break;
            case R.id.buttonCallActionActivity:

                String strCall = "tel:" + m_strPhone.trim() ;

                Intent callIntent = new Intent();
                //callIntent.setAction(Intent.ACTION_CALL);
                callIntent.setAction(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse(strCall));
                startActivity(callIntent);

                break;
            case R.id.buttonSendEmailActionActivity:
                EditText subjectEditText = (EditText) findViewById(R.id.editTextSubjectActionActivity);
                EditText contentEditText = (EditText) findViewById(R.id.editTextContentActionActivity);

                String strSubject = subjectEditText.getText().toString();
                String strContent = contentEditText.getText().toString();

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/html");
                intent.putExtra(Intent.EXTRA_EMAIL , new String[]{m_strEmail});
                intent.putExtra(Intent.EXTRA_SUBJECT, strSubject);
                intent.putExtra(Intent.EXTRA_TEXT, strContent);
                startActivity(Intent.createChooser(intent, "Send email..."));
                break;
        }

    }
}
