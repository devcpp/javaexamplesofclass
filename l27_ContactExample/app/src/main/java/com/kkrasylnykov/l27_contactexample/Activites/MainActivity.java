package com.kkrasylnykov.l27_contactexample.Activites;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.kkrasylnykov.l27_contactexample.Adapter.ContactInfoAdapter;
import com.kkrasylnykov.l27_contactexample.Model.ContactInfo;
import com.kkrasylnykov.l27_contactexample.Model.Wrapper.ContactInfoContactWrapper;
import com.kkrasylnykov.l27_contactexample.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ContactInfoAdapter m_adapter = null;
    private ListView m_list = null;
    private EditText m_searchEditText = null;

    private ArrayList<ContactInfo> m_arrData = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_arrData = new ArrayList<>();

        m_searchEditText = (EditText) findViewById(R.id.editTextSearchMainActivity);
        m_searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String strSearch = charSequence.toString();
                m_arrData.clear();
                ContactInfoContactWrapper wrapper = new ContactInfoContactWrapper(MainActivity.this);
                m_arrData.addAll(wrapper.getInfoBySearch(strSearch));
                m_adapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        m_adapter = new ContactInfoAdapter(m_arrData);
        m_list = (ListView) findViewById(R.id.listViewMainActivity);
        m_list.setAdapter(m_adapter);
        m_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d("devcppl27","onItemClick");

                ContactInfo info = (ContactInfo) m_adapter.getItem(i);

                Intent intent = new Intent(MainActivity.this, ActionActivity.class);
                intent.putExtra(ActionActivity.KEY_NAME, info.getDisplayName());
                intent.putExtra(ActionActivity.KEY_PHONE, info.getPhone());
                intent.putExtra(ActionActivity.KEY_EMAIL, info.getEmail());
                startActivity(intent);
            }
        });
    }
}
