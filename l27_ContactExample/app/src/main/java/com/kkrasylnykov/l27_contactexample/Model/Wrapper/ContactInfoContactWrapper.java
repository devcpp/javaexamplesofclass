package com.kkrasylnykov.l27_contactexample.Model.Wrapper;


import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import com.kkrasylnykov.l27_contactexample.Model.ContactInfo;

import java.util.ArrayList;

public class ContactInfoContactWrapper {

    private Context m_Context = null;

    public ContactInfoContactWrapper(Context context){
        m_Context = context;
    }

    public ArrayList<ContactInfo> getInfoBySearch(String strSearch){
        ArrayList<ContactInfo> arrResult = new ArrayList<>();

        /*arrResult.add(new ContactInfo("KOs Kras","095","k@g.c"));
        arrResult.add(new ContactInfo("Test","123","d564@y.c"));
        arrResult.add(new ContactInfo("qaz","457@g.c","des"));*/

        String[] PROJECTION = new String[] { ContactsContract.RawContacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Email.DATA,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID,
                ContactsContract.Contacts.HAS_PHONE_NUMBER};


        String strQuery = ContactsContract.CommonDataKinds.Email.DATA + " LIKE ?";
        String[] arrParams = new String[]{"%"+strSearch+"%"};
        Cursor cursor = m_Context.getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                PROJECTION, strQuery, arrParams, null);

        if (cursor!= null && cursor.moveToFirst()){
            do{
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
                String strName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                String strEmail = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                if(Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0){
                    Uri queryUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
                    String[] phoneProjection = new String[] {ContactsContract.CommonDataKinds.Phone.NUMBER};
                    String strPhoneQuery = ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?";
                    String[] arrPhoneParams = new String[]{id};

                    Cursor pCur = m_Context.getContentResolver().query(queryUri, phoneProjection,
                            strPhoneQuery, arrPhoneParams, null);
                    if (pCur != null && pCur.moveToFirst()) {
                        do {
                            String contactNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            arrResult.add(new ContactInfo(strName, contactNumber, strEmail));
                        } while (pCur.moveToNext());
                        pCur.close();
                    }
                }
            } while (cursor.moveToNext());
            cursor.close();
        }
        return arrResult;
    }
}
