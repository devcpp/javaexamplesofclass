package com.kkrasylnykov.l27_contactexample.Model;

public class ContactInfo {

    private String m_strDisplayName = "";
    private String m_strPhone = "";
    private String m_strEmail = "";

    public ContactInfo(String strDisplayName, String strPhone, String strEmail) {
        this.m_strDisplayName = strDisplayName;
        this.m_strPhone = strPhone;
        this.m_strEmail = strEmail;
    }

    public String getDisplayName() {
        return m_strDisplayName;
    }

    public void setDisplayName(String m_strDisplayName) {
        this.m_strDisplayName = m_strDisplayName;
    }

    public String getPhone() {
        return m_strPhone;
    }

    public void setPhone(String m_strPhone) {
        this.m_strPhone = m_strPhone;
    }

    public String getEmail() {
        return m_strEmail;
    }

    public void setEmail(String m_strEmail) {
        this.m_strEmail = m_strEmail;
    }
}
