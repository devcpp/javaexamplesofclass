package com.kkrasylnykov.l27_contactexample.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kkrasylnykov.l27_contactexample.Model.ContactInfo;
import com.kkrasylnykov.l27_contactexample.R;

import java.util.ArrayList;

public class ContactInfoAdapter extends BaseAdapter {

    private ArrayList<ContactInfo> m_arrData = null;

    public ContactInfoAdapter(ArrayList<ContactInfo> arrData){
        m_arrData = arrData;
    }

    @Override
    public int getCount() {
        return m_arrData.size();
    }

    @Override
    public Object getItem(int i) {
        return m_arrData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return -1;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view==null){
            LayoutInflater li = (LayoutInflater)viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(R.layout.item_contact_info, viewGroup, false);
        }

        ContactInfo info = (ContactInfo) getItem(i);

        TextView displayTextView = (TextView) view.findViewById(R.id.textViewDisplayName);
        displayTextView.setText(info.getDisplayName());

        TextView phoneTextView = (TextView) view.findViewById(R.id.textViewPhone);
        phoneTextView.setText(info.getPhone());

        TextView emailTextView = (TextView) view.findViewById(R.id.textViewEmail);
        emailTextView.setText(info.getEmail());
        return view;
    }
}
