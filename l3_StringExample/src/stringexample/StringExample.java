package stringexample;

import java.util.Scanner;

public class StringExample {

    public static void main(String[] args) {
        String[] arrNums = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
        
        Scanner scan = new Scanner(System.in);
        
        String str = scan.nextLine();
        
        for (String strNum:arrNums){
             str = str.replace(strNum, " ");
        }
        
        System.out.println("Result: " + str);
        
    }
    
}
