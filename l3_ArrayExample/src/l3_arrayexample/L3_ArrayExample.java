
package l3_arrayexample;

import java.util.Scanner;


public class L3_ArrayExample {


    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        int nCount = -1;
        
        System.out.println("Введите колличество сторон: ");
        nCount = scan.nextInt();
        
        float[] arr = new float[nCount];
        
        for(int i=0; i<arr.length;i++){
            int nNum = i +1;
            System.out.print("Введите " + nNum + " длинну :");
            arr[i] = scan.nextFloat();
        }
        
        float fSum = 0.0f;
        
        for(float fP:arr){
            fSum+=fP;
        }
        
        System.out.println("Сумма: " + fSum);
    }
    
}
