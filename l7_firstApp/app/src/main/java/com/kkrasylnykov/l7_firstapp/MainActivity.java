package com.kkrasylnykov.l7_firstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String PASSWORD = "Spalah";

    private static final int ACTIVITY_SECOND = 1234;

    private static final long TIME_ALERT = new Long(1465733075)+10*60;

    private TextView m_textView = null;
    private EditText m_editText = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("devcpp", "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_textView = (TextView) findViewById(R.id.textViewMainActivity);
        m_textView.setText("Измененный текст");

        m_editText = (EditText) findViewById(R.id.editTextMainActivity);
        m_editText.setHint("Введите пароль:");

        Button button = (Button)findViewById(R.id.button_new);
        button.setText(getResources().getString(R.string.button_name));
        button.setOnClickListener(this);

        Button buttonNextActivity = (Button)findViewById(R.id.nextActivityButton);
        buttonNextActivity.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String strText = m_editText.getText().toString();
        switch (v.getId()){
            case R.id.button_new:

                Log.d("devcpp", "onClick -> " + strText);
                if (strText.equals(PASSWORD)){
                    m_textView.setText("Ура!!!! Вы ввели правильный пароль!!!");
                } else {
                    m_textView.setText("Вы самозванец!!!");
                }
                break;
            case R.id.nextActivityButton:

                Intent intent = new Intent(this,SecondActivity.class);
                intent.putExtra(SecondActivity.KEY_PASSWORD,strText);
                startActivityForResult(intent, ACTIVITY_SECOND);
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("devcpp", "requestCode -> " + requestCode +"<>" + ACTIVITY_SECOND);
        Log.d("devcpp", "resultCode -> " + resultCode +"<>" + RESULT_OK);
        if(requestCode==ACTIVITY_SECOND){
            if (resultCode==RESULT_OK){
                m_textView.setText("Все хорошо!");
            } else {
                if (data!=null){
                    String strText = data.getExtras().getString(SecondActivity.KEY_NAME,"");
                    m_textView.setText("Что-то пошло не так! Виновник " + strText);
                } else {
                    throw new RuntimeException("Intent data is null");
                }

            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        long nTime  = System.currentTimeMillis()/1000;
        Log.d("devcpp", "onResume -> " + nTime + "<<>>" + TIME_ALERT);
        if(nTime>TIME_ALERT){
            //m_textView.setText("Время настало!!!!");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("devcpp", "onStart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("devcpp", "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("devcpp", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("devcpp", "onDestroy");
    }
}
