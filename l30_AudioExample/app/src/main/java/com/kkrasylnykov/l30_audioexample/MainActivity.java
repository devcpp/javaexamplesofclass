package com.kkrasylnykov.l30_audioexample;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private final static String DATA_HTTP = "http://dl.dropboxusercontent.com/u/6197740/explosion.mp3";
    private final static String DATA_STREAM = "http://online.radiorecord.ru:8101/rr_128";
    private final static String DATA_SD = Environment
            .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            + "/song.mp3";

    private MediaPlayer m_mediaPlayer = null;
    private AudioManager m_audioManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnPlayLocalAudio = (Button) findViewById(R.id.btnPlayLocalAudio);
        btnPlayLocalAudio.setOnClickListener(this);

        Button btnPlayInetAudio = (Button) findViewById(R.id.btnPlayInetAudio);
        btnPlayInetAudio.setOnClickListener(this);

        Button btnPlayStreemAudio = (Button) findViewById(R.id.btnPlayStreemAudio);
        btnPlayStreemAudio.setOnClickListener(this);

        Button btnStopAudio = (Button) findViewById(R.id.btnStop);
        btnStopAudio.setOnClickListener(this);

        m_audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
    }

    @Override
    public void onClick(View view) {
        releaseMediaPlayer();
        switch (view.getId()){
            case R.id.btnPlayLocalAudio:
                m_mediaPlayer = new MediaPlayer();
                try {
                    m_mediaPlayer.setDataSource(DATA_SD);
                    m_mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    m_mediaPlayer.prepare();
                    m_mediaPlayer.start();
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this,"File not found " + DATA_SD, Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnPlayInetAudio:
                m_mediaPlayer = new MediaPlayer();
                try {
                    m_mediaPlayer.setDataSource(DATA_HTTP);
                    m_mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    m_mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mediaPlayer) {
                            m_mediaPlayer.start();
                        }
                    });
                    m_mediaPlayer.prepareAsync();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                break;
            case R.id.btnPlayStreemAudio:
                m_mediaPlayer = new MediaPlayer();
                try {
                    m_mediaPlayer.setDataSource(DATA_STREAM);
                    m_mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    m_mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mediaPlayer) {
                            m_mediaPlayer.start();
                        }
                    });
                    m_mediaPlayer.prepareAsync();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btnStop:
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseMediaPlayer();
    }

    private void releaseMediaPlayer() {
        if (m_mediaPlayer != null) {
            try {
                m_mediaPlayer.release();
                m_mediaPlayer = null;
            } finally {

            }
        }
    }
}
