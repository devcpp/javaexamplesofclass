package com.kkrasylnykov.l19_fileandpermisionexample.Model;

public class FileOnServer {
    String m_strServerUrl = "";
    String m_strLocalName = "";

    public FileOnServer(String m_strLocalName, String m_strServerUrl) {
        this.m_strServerUrl = m_strServerUrl;
        this.m_strLocalName = m_strLocalName;
    }

    public String getServerUrl() {
        return m_strServerUrl;
    }

    public void setServerUrl(String m_strServerUrl) {
        this.m_strServerUrl = m_strServerUrl;
    }

    public String getLocalName() {
        return m_strLocalName;
    }

    public void setLocalName(String m_strLocalName) {
        this.m_strLocalName = m_strLocalName;
    }
}
