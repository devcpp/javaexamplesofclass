package com.kkrasylnykov.l19_fileandpermisionexample.Acvtivites;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.kkrasylnykov.l19_fileandpermisionexample.Adapters.FileListAdapter;
import com.kkrasylnykov.l19_fileandpermisionexample.R;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    public static final int REQUEST_PERMISSION_ON_ATTACH_FILE = 1;

    ListView m_listView = null;
    ArrayList<File> m_arrData = null;
    FileListAdapter m_adapter = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Получение пути к директориям системы.
        //Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_ON_ATTACH_FILE);
        }else{
            onCreate();
        }

    }

    public void onCreate(){
        m_arrData = getJpgFiles(Environment.getExternalStorageDirectory());
        m_listView = (ListView) findViewById(R.id.MainListView);
        m_adapter = new FileListAdapter(m_arrData);
        m_listView.setAdapter(m_adapter);
        m_listView.setOnItemClickListener(this);
    }

    public ArrayList<File> getJpgFiles(File file){
        ArrayList<File> arrFiles = new ArrayList<>();
        if (file.exists()){
            if (file.isDirectory()){
                File[] arrTmpFiles = file.listFiles();
                for (File tmpFile:arrTmpFiles){
                    arrFiles.addAll(getJpgFiles(tmpFile));
                }
            } else if (file.isFile()){
                if(file.getName().contains(".jpg")){
                    arrFiles.add(file);
                }
            }
        }
        return arrFiles;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        File curFile = (File) m_adapter.getItem(i);
        String strFilePath = curFile.getAbsolutePath();
        Intent intent = new Intent(this, ViewActivity.class);
        intent.putExtra(ViewActivity.KEY_FILE_PATHS, arrayFilesToString(m_arrData));
        intent.putExtra(ViewActivity.KEY_POSITION, i);
        startActivity(intent);
    }

    public String[] arrayFilesToString(ArrayList<File> arrData){
        int nCount = arrData.size();
        String[] arrReturn = new String[nCount];
        for (int i=0; i<nCount; i++){
            arrReturn[i] = arrData.get(i).getAbsolutePath();
        }
        return arrReturn;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode==REQUEST_PERMISSION_ON_ATTACH_FILE){
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED){
                Toast.makeText(MainActivity.this, "Приложение не может работать без разрешения", Toast.LENGTH_LONG).show();
                finish();
            }else {
                onCreate();
            }
        }
    }
}
