package com.kkrasylnykov.l19_fileandpermisionexample.Acvtivites;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.kkrasylnykov.l19_fileandpermisionexample.Adapters.ImageViewFragmentAdapter;
import com.kkrasylnykov.l19_fileandpermisionexample.R;

public class ViewActivity extends AppCompatActivity {
    public final static String KEY_FILE_PATHS = "KEY_FILE_PATHS";
    public final static String KEY_POSITION = "KEY_POSITION";

    String[] m_arrPaths = null;

    ViewPager m_ViewPager = null;
    ImageViewFragmentAdapter m_adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        m_ViewPager = (ViewPager) findViewById(R.id.ViewPager);

        int nPosition = 0;

        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                m_arrPaths = bundle.getStringArray(KEY_FILE_PATHS);
                nPosition = bundle.getInt(KEY_POSITION);
            }
        }
        m_adapter = new ImageViewFragmentAdapter(getSupportFragmentManager(),m_arrPaths);
        m_ViewPager.setAdapter(m_adapter);
        m_ViewPager.setCurrentItem(nPosition);
    }
}
