
package l4_notepadexample;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;


public class L4_NotepadExample {

    public static void main(String[] args) {
        String KEY_NAME     = "KEY_NAME";
        String KEY_AGE      = "KEY_AGE";
        String KEY_PHONE    = "KEY_PHONE";
        String KEY_SEX      = "KEY_SEX";
        
        Scanner scan = new Scanner(System.in);
        
        ArrayList<HashMap<String,Object>> arrNotepadItems = new ArrayList<>();
        
        do{
            System.out.println("Menu:");
            System.out.println("1. Add item");
            System.out.println("2. Show items");
            System.out.println("3. Exit");
            
            int nVal = scan.nextInt();
            if(nVal == 1){
                scan.nextLine();
                System.out.println("strName :");
                String strName = scan.nextLine();
                System.out.println("nAge :");
                Integer nAge = scan.nextInt();
                scan.nextLine();
                System.out.println("strPhone :");
                String strPhone = scan.nextLine();
                System.out.println("nSex :");
                Integer nSex = scan.nextInt();
                
                HashMap<String,Object> item = new HashMap<String,Object>();
                item.put(KEY_NAME, strName);
                item.put(KEY_AGE, nAge);
                item.put(KEY_PHONE, strPhone);
                item.put(KEY_SEX, nSex);
                
                arrNotepadItems.add(item);
            } else if (nVal == 2){
                if(arrNotepadItems.size()>0){
                    for (int i=0; i< arrNotepadItems.size();i++){
                        HashMap<String,Object> item = arrNotepadItems.get(i);
                        String strName = (String)item.get(KEY_NAME);
                        Integer nAge = (Integer)item.get(KEY_AGE);
                        String strPhone = (String)item.get(KEY_PHONE);
                        Integer nSex = (Integer)item.get(KEY_SEX);
                        
                        System.out.println("Item : " + (i+1));
                        System.out.println("Name : " + strName);
                        System.out.println("Age : " + nAge);
                        System.out.println("Phone : " + strPhone);
                        System.out.println("Sex : " + nSex);
                        System.out.println();
                        System.out.println();
                    }
                } else {
                    System.out.println("List is empty!");
                }
                
            } else if (nVal == 3){
                break;
            }
            
        }while(true);
    }
    
}
