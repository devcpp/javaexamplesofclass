package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model;

import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.ToolsAndConstants.AppConstants;

import java.util.ArrayList;

public class UserInfo {
    private long m_nId = -1;
    private String m_strName = "";
    private String m_strSName = "";
    private ArrayList<PhoneInfo> m_arrPhones = new ArrayList<>();
    private String m_strAddress = "";
    private int m_nBYear = -1;

    private String m_strFullName = "";

    public UserInfo(Cursor cursor){
        setId(cursor.getLong(0));
        setName(cursor.getString(1));
        setSName(cursor.getString(2));
        setAddress(cursor.getString(3));
        setBYear(cursor.getInt(4));
    }

    public UserInfo(String strName, String strSName, ArrayList<PhoneInfo> arrPhones, String strAddress, int nBYear){
        setName(strName);
        setSName(strSName);
        setPhones(arrPhones);
        setAddress(strAddress);
        setBYear(nBYear);
    }

    public long getId() {
        return m_nId;
    }

    public void setId(long nId) {
        m_nId = nId;
    }

    public String getName() {
        return m_strName;
    }

    public void setName(String m_strName) {
        this.m_strName = m_strName;
    }

    public String getSName() {
        return m_strSName;
    }

    public void setSName(String m_strSName) {
        this.m_strSName = m_strSName;
    }

    public ArrayList<PhoneInfo> getPhones() {
        return m_arrPhones;
    }

    public void setPhones(ArrayList<PhoneInfo> arrPhones) {
        this.m_arrPhones = arrPhones;
    }

    public String getAddress() {
        return m_strAddress;
    }

    public void setAddress(String m_strAddress) {
        this.m_strAddress = m_strAddress;
    }

    public int getBYear() {
        return m_nBYear;
    }

    public void setBYear(int m_nBYear) {
        this.m_nBYear = m_nBYear;
    }

    public String getFullName() {
        return getName() + " " + getSName();
    }

    public ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        values.put(AppConstants.DB_TABLE_USERS.FIELD_NAME, getName());
        values.put(AppConstants.DB_TABLE_USERS.FIELD_SNAME,getSName());
        values.put(AppConstants.DB_TABLE_USERS.FIELD_ADDRESS,getAddress());
        values.put(AppConstants.DB_TABLE_USERS.FIELD_BYEAR,getBYear());
        return values;
    }
}
