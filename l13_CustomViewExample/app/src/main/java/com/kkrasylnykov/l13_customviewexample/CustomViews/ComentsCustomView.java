package com.kkrasylnykov.l13_customviewexample.CustomViews;


import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.kkrasylnykov.l13_customviewexample.R;

public class ComentsCustomView extends FrameLayout {

    private ImageView m_iw = null;
    private TextView m_tw = null;


    public ComentsCustomView(Context context) {
        super(context);
        initView(context);
    }

    public ComentsCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public ComentsCustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context){
        LayoutInflater.from(context).inflate(R.layout.custom_view_coments,this,true);
        m_tw = (TextView) findViewById(R.id.textViewCV);
        m_iw = (ImageView) findViewById(R.id.imageViewCV);
    }

    public void setData(int nIdImage, String strText){
        m_tw.setText(strText);
        m_iw.setImageResource(nIdImage);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int nWidth = MeasureSpec.getSize(widthMeasureSpec);

        int nWidthIV = m_iw.getMeasuredWidth();

        int nWidthTV = nWidth - nWidthIV;
        int nMaxHeightTV = m_iw.getMeasuredHeight();

        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)m_tw.getLayoutParams();
        params.setMargins(nWidthIV+5,0,0,0);

        m_tw.setMaxWidth(nWidthTV);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        while(m_tw.getMeasuredHeight()>nMaxHeightTV){
            float fTextSize = m_tw.getTextSize() - 1;
            m_tw.setTextSize(TypedValue.COMPLEX_UNIT_PX,fTextSize);

            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }



    }
}
