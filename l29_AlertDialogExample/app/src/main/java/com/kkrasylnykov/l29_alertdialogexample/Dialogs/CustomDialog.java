package com.kkrasylnykov.l29_alertdialogexample.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.kkrasylnykov.l29_alertdialogexample.R;

public class CustomDialog extends Dialog implements View.OnClickListener {

    private DialogInterface.OnClickListener m_onClickListener = null;
    private OnClickCustomDialogButton m_onClickListenerButton = null;
    private EditText m_infoEditText = null;

    protected CustomDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public CustomDialog(Context context) {
        super(context);
    }

    public CustomDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_custom);

        getWindow().setBackgroundDrawable(new ColorDrawable(0));

        m_infoEditText = (EditText) findViewById(R.id.InfoEditTextCustomDialog);
        Button button = (Button) findViewById(R.id.SetInfoButtonCustomDialog);
        button.setOnClickListener(this);
    }

    public void setButtonOnClickListener(DialogInterface.OnClickListener buttonOnClickListener){
        m_onClickListener = buttonOnClickListener;
    }

    public void setButtonOnClickListener(OnClickCustomDialogButton buttonOnClickListener){
        m_onClickListenerButton = buttonOnClickListener;
    }

    public String getText(){
        return m_infoEditText.getText().toString();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.SetInfoButtonCustomDialog:
                /*if (m_onClickListener!=null){
                    m_onClickListener.onClick(this,R.id.SetInfoButtonCustomDialog);
                }*/
                if (m_onClickListenerButton!=null){
                    m_onClickListenerButton.onClick(getText());
                }
                dismiss();
                break;
        }
    }

    public interface OnClickCustomDialogButton{
        public void onClick(String strText);
    }
}
