package com.kkrasylnykov.l29_alertdialogexample.Activites;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.kkrasylnykov.l29_alertdialogexample.Dialogs.CustomDialog;
import com.kkrasylnykov.l29_alertdialogexample.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button m_btnCustomDialog = null;
    Button m_btnDateDialog = null;
    Button m_btnTimeDialog = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnAllertDialog = (Button) findViewById(R.id.AlertDialog);
        btnAllertDialog.setOnClickListener(this);

        Button btnProgressDialog = (Button) findViewById(R.id.ProgressDialog);
        btnProgressDialog.setOnClickListener(this);

        m_btnCustomDialog = (Button) findViewById(R.id.CustomDialog);
        m_btnCustomDialog.setOnClickListener(this);

        m_btnDateDialog = (Button) findViewById(R.id.DateDialog);
        m_btnDateDialog.setOnClickListener(this);

        m_btnTimeDialog = (Button) findViewById(R.id.TimeDialog);
        m_btnTimeDialog.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        final Calendar calendar = Calendar.getInstance();
        switch (view.getId()){
            case R.id.AlertDialog:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Warning!!");
                builder.setMessage("Вы уверены?");
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (dialogInterface!=null){
                            dialogInterface.dismiss();
                        }

                        Toast.makeText(MainActivity.this, "Вы нажали на PositiveButton -> " + i, Toast.LENGTH_LONG).show();
                    }
                });

                builder.setNegativeButton("Discard", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (dialogInterface!=null){
                            dialogInterface.dismiss();
                        }

                        Toast.makeText(MainActivity.this, "Вы нажали на NegativeButton -> " + i, Toast.LENGTH_LONG).show();
                    }
                });

                builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (dialogInterface!=null){
                            dialogInterface.dismiss();
                        }

                        Toast.makeText(MainActivity.this, "Вы нажали на NeutralButton -> " + i, Toast.LENGTH_LONG).show();
                    }
                });

                builder.setCancelable(false);
                builder.show();
                break;
            case R.id.CustomDialog:
                CustomDialog customDialog = new CustomDialog(this);
                /*customDialog.setButtonOnClickListener(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String strText = ((CustomDialog)dialogInterface).getText();
                        m_btnCustomDialog.setText(strText);
                    }
                });*/
                customDialog.setButtonOnClickListener(new CustomDialog.OnClickCustomDialogButton() {
                    @Override
                    public void onClick(String strText) {
                        m_btnCustomDialog.setText(strText);
                    }
                });
                customDialog.show();
                break;
            case R.id.DateDialog:
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE, dd.MM.yyyy");
                        Date date = new Date((i-1900), i1, i2);
                        m_btnDateDialog.setText(simpleDateFormat.format(date));

                        //String strMon = i1<=8?"0"+(i1+1):Integer.toString(i1+1);
                        //m_btnDateDialog.setText("Date: " + i2 + "." + strMon + "." + i);
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
                break;
            case R.id.TimeDialog:
                TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
                        Date date = new Date((calendar.get(Calendar.YEAR)-1900), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), i, i1);
                        m_btnTimeDialog.setText(simpleDateFormat.format(date));
                    }
                }, calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE),true);
                timePickerDialog.show();
                break;

            case R.id.ProgressDialog:
                ProgressDialog progressDialog = new ProgressDialog(this);
                //progressDialog.setMessage("Loading...");
                progressDialog.setCancelable(false);
                progressDialog.show();
                break;
        }
    }
}
