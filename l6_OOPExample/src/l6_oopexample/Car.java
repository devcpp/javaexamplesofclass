
package l6_oopexample;

import java.util.Scanner;

public class Car extends BaseTransport{
    
    public static final int CARCASE_SEDAN = 1;
    public static final int CARCASE_UNIVERSAL = 2;
    public static final int CARCASE_LIFTBACK = 3;
    
    private int m_nCarCase = CARCASE_SEDAN;

    public Car() {
        super(TYPE_CAR);
    }

    @Override
    public void insertData() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите название производителя машины:");
        String strCompanyName = scan.nextLine();
        System.out.print("Введите цвет машины:");
        String strColor = scan.nextLine();
        System.out.print("Введите мощность двигателя машины:");
        float fDPower = scan.nextFloat();
        System.out.print("Введите кол-во колес машины:");
        int nCountWheel = scan.nextInt();
        System.out.print("Введите диаметр колес машины:");
        int nDWheel = scan.nextInt();
        System.out.print("Какой тип кузова?\n1. Седан\n2.Универсал\n3. ЛифтБек\nСовершите выбор:");
        int nCarcese = scan.nextInt();
        System.out.print("Какой тип топлива?\n1. Газ\n2.Дизель\n3. Бензин\nСовершите выбор:");
        int nTypeFuel = scan.nextInt();
        
        setCompanyName(strCompanyName);
        setColor(strColor);
        setPowerD(fDPower);
        setCountWheel(nCountWheel);
        setDWheel(nDWheel);
        setCarCase(nCarcese);
        setTypeFuel(nTypeFuel);
        
    }

    @Override
    public void outData() {
        System.out.print("Автомобиль с кузовом ");
        switch(getCarCase()){
            case CARCASE_UNIVERSAL:
                System.out.print("универсал, ");
                break;
            case CARCASE_SEDAN:
                System.out.print("седан, ");
                break;
            case CARCASE_LIFTBACK:
                System.out.print("лифтбек, ");
                break;
        }
        System.out.print(getCompanyName() + ", мощность " + getPowerD() + " Ватт, цвет "
                + getColor() + ", " + getCountWheel() + " колес диаметром" + getDWheel() + ", топливо ");
        switch(getTypeFuel()){
            case TYPE_FUEL_GAS:
                System.out.println("газ");
                break;
            case TYPE_FUEL_DIESEL:
                System.out.println("дизель");
                break;
            case TYPE_FUEL_PETROL:
                System.out.println("бензин");
                break;
        }
    }
    
    public int getCarCase(){
        return m_nCarCase;
    }

    @Override
    public void setCompanyName(String strCompanyName) {
        strCompanyName = "Car manufacturer " + strCompanyName;
        super.setCompanyName(strCompanyName);
    }
    
    public void setCarCase(int nCarCase){
        m_nCarCase = nCarCase;
    }
    
}
