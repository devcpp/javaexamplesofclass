package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.Wrappers.DBWrappers;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.ToolsAndConstants.DbHelper;

public class BaseDBWrapper {

    private  DbHelper m_DbHelper = null;
    private  String m_strTableName = "";

    public BaseDBWrapper(Context Context, String strTableName){
        m_DbHelper = new DbHelper(Context);
        m_strTableName = strTableName;
    }

    public SQLiteDatabase getDBRead(){
        return m_DbHelper.getDBRead();
    }

    public SQLiteDatabase getDBWrite(){
        return m_DbHelper.getDBWrite();
    }

    public String getTableName() {
        return m_strTableName;
    }
}
