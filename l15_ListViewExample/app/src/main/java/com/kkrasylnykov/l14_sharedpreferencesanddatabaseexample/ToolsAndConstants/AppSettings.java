package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.ToolsAndConstants;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppSettings {

    private static final String KEY_BOOLEAN_IS_FIRST_START = "KEY_IS_FIRST_START";

    private static SharedPreferences getSharedPreferences(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static boolean getIsFirstStart(Context context){
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        boolean bResult = sharedPreferences.getBoolean(KEY_BOOLEAN_IS_FIRST_START,true);
        return bResult;
    }

    public static void setIsFirstStart(Context context, boolean bIsFirstStart){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(KEY_BOOLEAN_IS_FIRST_START, bIsFirstStart);
        editor.commit();

    }
}

