package com.kkrasylnykov.l28_galleryandcameraexample;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final static int OPEN_GALLERY = 1101;
    private final static int REQUEST_CODE_PHOTO = 1102;

    private File m_directory = null;

    private Uri m_uri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buttonGallery = (Button) findViewById(R.id.buttonLoadGallery);
        Button buttonPhoto = (Button) findViewById(R.id.buttonPhoto);

        buttonGallery.setOnClickListener(this);
        buttonPhoto.setOnClickListener(this);

        m_directory = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "l28Example");
        if (!m_directory.exists()){
            m_directory.mkdirs();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonLoadGallery:
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, OPEN_GALLERY);

                break;
            case R.id.buttonPhoto:
                Intent intentPhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //m_uri = generateFileUri();
                //intentPhoto.putExtra(MediaStore.EXTRA_OUTPUT, m_uri);
                startActivityForResult(intentPhoto, REQUEST_CODE_PHOTO);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==OPEN_GALLERY){
            if (resultCode==RESULT_OK){
                Uri ChossefileUri = data.getData();
                if(ChossefileUri !=null){
                    ImageView imageView = (ImageView) findViewById(R.id.imageView);
                    imageView.setImageURI(ChossefileUri);
                }
            }
        } else if (requestCode==REQUEST_CODE_PHOTO){
            if (resultCode==RESULT_OK){
                if (data!=null) {
                    Bundle bndl = data.getExtras();
                    if (bndl != null) {
                        Object obj = data.getExtras().get("data");
                        if (obj instanceof Bitmap) {
                            Bitmap bitmap = (Bitmap) obj;
                            Log.d("devcppl28", "bitmap " + bitmap.getWidth() + " x "
                                    + bitmap.getHeight());
                            ImageView imageView = (ImageView) findViewById(R.id.imageView);
                            imageView.setImageBitmap(bitmap);
                        }
                    }
                } else if (m_uri!=null) {
                    Uri ChossefileUri = m_uri;
                    if(ChossefileUri !=null){
                        ImageView imageView = (ImageView) findViewById(R.id.imageView);
                        imageView.setImageURI(ChossefileUri);
                    }
                }
                /**/

            }
        }
    }

    private Uri generateFileUri() {
        File file = new File(m_directory.getPath() + "/" + "photo_"
                + System.currentTimeMillis() + ".jpg");
        return Uri.fromFile(file);
    }
}
