package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Activites;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Adapters.NotepadExpandableAdapter;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.Engines.UserInfoEngine;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.UserInfo;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.R;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.ToolsAndConstants.AppSettings;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, NotepadExpandableAdapter.OnClickGroupListener {

    public static final int MENU_ITEM_TIMER     = 101;
    public static final int MENU_ITEM_GRAPH     = 102;
    public static final int MENU_ITEM_DEL       = 103;
    public static final int MENU_ITEM_HELP      = 104;

    EditText m_searchEditText = null;
    Button m_addButton = null;
    ExpandableListView m_ExpandableListView = null;

    NotepadExpandableAdapter m_adapter = null;

    ArrayList<UserInfo> m_arrData = new ArrayList<>();
    String m_strSearch = "";

    DrawerLayout m_DrawerLayout = null;
    View m_NavigationDrawer = null;

    Toolbar m_toolbar = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(AppSettings.getIsFirstStart(this)){
            AppSettings.setIsFirstStart(this, false);
            Toast.makeText(this,"This first start APP!!!!",Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this,"This NOT first start APP!!!!",Toast.LENGTH_LONG).show();
        }

        m_toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(m_toolbar);

        getSupportActionBar().setTitle("Окно");

        Drawable menuIconDrawable = ContextCompat.getDrawable(this, R.drawable.icon_left_menu);
        menuIconDrawable.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        m_toolbar.setNavigationIcon(menuIconDrawable);

        m_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_DrawerLayout.openDrawer(m_NavigationDrawer);
            }
        });

        Drawable menuRightIconDrawable = ContextCompat.getDrawable(this, R.drawable.icon_right_menu);
        menuRightIconDrawable.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        m_toolbar.setOverflowIcon(menuRightIconDrawable);

        m_DrawerLayout = (DrawerLayout) findViewById(R.id.DrawerLayoutMainActivity);
        m_NavigationDrawer = findViewById(R.id.NavigationDrawer);

        m_addButton = (Button) findViewById(R.id.AddInfoButton);
        m_addButton.setOnClickListener(this);
        Button removeAllInfoButton = (Button) findViewById(R.id.RemoveAllInfoButton);
        removeAllInfoButton.setOnClickListener(this);

        Button showNavigationBar = (Button) findViewById(R.id.ShowNavigationBar);
        showNavigationBar.setOnClickListener(this);

        m_searchEditText = (EditText) findViewById(R.id.SearchEditText);
        m_searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                m_strSearch = s.toString();
                showList();
            }
        });
        m_ExpandableListView = (ExpandableListView) findViewById(R.id.mainExpandableListView);
        m_adapter = new NotepadExpandableAdapter(m_arrData);
        m_ExpandableListView.setAdapter(m_adapter);
        m_adapter.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();

        MenuItem itemTime = menu.add(0, MENU_ITEM_TIMER,0,"Timer");

        Drawable drawableIconTimer = ContextCompat.getDrawable(this, R.drawable.icon_timer);
        drawableIconTimer.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        itemTime.setIcon(drawableIconTimer);
        itemTime.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        MenuItem itemGraph = menu.add(1, MENU_ITEM_GRAPH,1,"Graph");
        Drawable drawableIconGraph = ContextCompat.getDrawable(this, R.drawable.icon_graph);
        drawableIconGraph.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        itemGraph.setIcon(drawableIconGraph);
        itemGraph.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        MenuItem itemDel = menu.add(3, MENU_ITEM_DEL,3,"Del");
        itemDel.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        MenuItem itemHelp = menu.add(4, MENU_ITEM_HELP,4,"Help");
        itemHelp.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case MENU_ITEM_DEL:
                Toast.makeText(this, "Click MENU_ITEM_DEL", Toast.LENGTH_LONG).show();
                break;
            case MENU_ITEM_GRAPH:
                Toast.makeText(this, "Click MENU_ITEM_GRAPH", Toast.LENGTH_LONG).show();
                break;
            case MENU_ITEM_HELP:
                Toast.makeText(this, "Click MENU_ITEM_HELP", Toast.LENGTH_LONG).show();
                break;
            case MENU_ITEM_TIMER:
                Toast.makeText(this, "Click MENU_ITEM_TIMER", Toast.LENGTH_LONG).show();
                break;
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        showList();
    }

    private void showList(){
        UserInfoEngine engine = new UserInfoEngine(this);
        m_arrData.clear();
        if(m_strSearch.isEmpty()){
            m_arrData.addAll(engine.getAll());
        } else {
            m_arrData.addAll(engine.getBySearchString(m_strSearch));
        }
        m_adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.AddInfoButton:
                Intent intent = new Intent(this, ComposerActivity.class);
                startActivity(intent);
                break;
            case R.id.RemoveAllInfoButton:
                UserInfoEngine engine = new UserInfoEngine(this);
                engine.removeAll();
                showList();
                break;
            case R.id.ShowNavigationBar:
                m_DrawerLayout.openDrawer(m_NavigationDrawer);
                break;
        }
    }

    @Override
    public void onClickGroup(long nId) {
        Intent intent = new Intent(this, ComposerActivity.class);
        intent.putExtra(ComposerActivity.KEY_RECORD_ID, nId);
        startActivity(intent);
    }
}
