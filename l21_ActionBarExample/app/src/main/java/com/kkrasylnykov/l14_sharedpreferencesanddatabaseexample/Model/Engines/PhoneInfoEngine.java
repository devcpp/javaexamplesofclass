package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.Engines;

import android.content.Context;

import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.PhoneInfo;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.UserInfo;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.Wrappers.DBWrappers.PhoneInfoDBWrapper;

import java.util.ArrayList;

public class PhoneInfoEngine extends BaseEngine {
    public PhoneInfoEngine(Context context) {
        super(context);
    }

    public void insert(PhoneInfo item){
        PhoneInfoDBWrapper wrapper = new PhoneInfoDBWrapper(getContext());
        wrapper.insert(item);
    }

    public void remove(PhoneInfo item){
        PhoneInfoDBWrapper wrapper = new PhoneInfoDBWrapper(getContext());
        wrapper.remove(item);
    }

    public void removeAll(){
        PhoneInfoDBWrapper wrapper = new PhoneInfoDBWrapper(getContext());
        wrapper.removeAll();
    }

    public void update(PhoneInfo item){
        PhoneInfoDBWrapper wrapper = new PhoneInfoDBWrapper(getContext());
        wrapper.update(item);
    }

    public ArrayList<PhoneInfo> getPhoneByUserId(long nUserId){
        PhoneInfoDBWrapper wrapper = new PhoneInfoDBWrapper(getContext());
        return wrapper.getPhoneByUserId(nUserId);
    }
}
