package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model;

import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.ToolsAndConstants.AppConstants;

public class PhoneInfo {
    private long m_nId = -1;
    private long m_nUserId = -1;
    private String m_strPhone = "";

    public PhoneInfo(long nId, long nUserId, String strPhone) {
        this.m_nId = nId;
        this.m_nUserId = nUserId;
        this.m_strPhone = strPhone;
    }

    public PhoneInfo(Cursor cursor) {
        setId(cursor.getLong(0));
        setUserId(cursor.getLong(1));
        setPhone(cursor.getString(2));
    }

    public long getId() {
        return m_nId;
    }

    public void setId(long m_nId) {
        this.m_nId = m_nId;
    }

    public long getUserId() {
        return m_nUserId;
    }

    public void setUserId(long m_nUserId) {
        this.m_nUserId = m_nUserId;
    }

    public String getPhone() {
        return m_strPhone;
    }

    public void setPhone(String m_strPhone) {
        this.m_strPhone = m_strPhone;
    }

    public ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        values.put(AppConstants.DB_TABLE_PHONES.FIELD_USER_ID, getUserId());
        values.put(AppConstants.DB_TABLE_PHONES.FIELD_PHONE,getPhone());
        return values;
    }
}
