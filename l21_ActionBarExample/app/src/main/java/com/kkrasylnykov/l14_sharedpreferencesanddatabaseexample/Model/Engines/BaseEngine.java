package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.Engines;

import android.content.Context;

public class BaseEngine {
    private Context m_Context = null;

    public BaseEngine(Context context){
        m_Context = context;
    }

    public Context getContext() {
        return m_Context;
    }
}
