package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.Engines;

import android.content.Context;

import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.UserInfo;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.Wrappers.DBWrappers.UserInfoDBWrapper;

import java.util.ArrayList;

public class UserInfoEngine extends BaseEngine {
    public UserInfoEngine(Context context) {
        super(context);
    }

    public void insert(UserInfo item){
        UserInfoDBWrapper wrapper = new UserInfoDBWrapper(getContext());
        wrapper.insert(item);
    }

    public void remove(UserInfo item){
        UserInfoDBWrapper wrapper = new UserInfoDBWrapper(getContext());
        wrapper.remove(item);
    }

    public void removeAll(){
        UserInfoDBWrapper wrapper = new UserInfoDBWrapper(getContext());
        wrapper.removeAll();
    }

    public void update(UserInfo item){
        UserInfoDBWrapper wrapper = new UserInfoDBWrapper(getContext());
        wrapper.update(item);
    }

    public ArrayList<UserInfo> getAll(){
        UserInfoDBWrapper wrapper = new UserInfoDBWrapper(getContext());
        return wrapper.getAll();
    }

    public ArrayList<UserInfo> getBySearchString(String strSearch){
        UserInfoDBWrapper wrapper = new UserInfoDBWrapper(getContext());
        return wrapper.getBySearchString(strSearch);
    }
}
