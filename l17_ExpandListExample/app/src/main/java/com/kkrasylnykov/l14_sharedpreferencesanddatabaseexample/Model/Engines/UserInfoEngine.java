package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.Engines;

import android.content.Context;

import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.PhoneInfo;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.UserInfo;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.Wrappers.DBWrappers.UserInfoDBWrapper;

import java.util.ArrayList;

public class UserInfoEngine extends BaseEngine {
    public UserInfoEngine(Context context) {
        super(context);
    }

    public void insert(UserInfo item){
        UserInfoDBWrapper wrapper = new UserInfoDBWrapper(getContext());
        long nUserId = wrapper.insert(item);
        ArrayList<PhoneInfo> arrPhones = item.getPhones();
        PhoneInfoEngine phoneInfoEngine = new PhoneInfoEngine(getContext());
        for (PhoneInfo phoneInfo:arrPhones){
            phoneInfo.setUserId(nUserId);
            phoneInfoEngine.insert(phoneInfo);
        }
    }

    public void remove(UserInfo item){
        UserInfoDBWrapper wrapper = new UserInfoDBWrapper(getContext());
        wrapper.remove(item);
        PhoneInfoEngine phoneInfoEngine = new PhoneInfoEngine(getContext());
        ArrayList<PhoneInfo> arrPhones = item.getPhones();
        for (PhoneInfo phoneInfo:arrPhones){
            phoneInfoEngine.remove(phoneInfo);
        }

    }

    public void removeAll(){
        UserInfoDBWrapper wrapper = new UserInfoDBWrapper(getContext());
        wrapper.removeAll();
        PhoneInfoEngine phoneInfoEngine = new PhoneInfoEngine(getContext());
        phoneInfoEngine.removeAll();
    }

    public void update(UserInfo item){
        UserInfoDBWrapper wrapper = new UserInfoDBWrapper(getContext());
        wrapper.update(item);
        PhoneInfoEngine phoneInfoEngine = new PhoneInfoEngine(getContext());
        ArrayList<PhoneInfo> arrPhones = item.getPhones();
        for (PhoneInfo phoneInfo:arrPhones){
            phoneInfoEngine.update(phoneInfo);
        }
    }

    public ArrayList<UserInfo> getAll(){
        UserInfoDBWrapper wrapper = new UserInfoDBWrapper(getContext());
        ArrayList<UserInfo> arrResult = wrapper.getAll();
        PhoneInfoEngine phoneInfoEngine = new PhoneInfoEngine(getContext());
        for(UserInfo userInfo: arrResult){
            long nUserId = userInfo.getId();
            userInfo.setPhones(phoneInfoEngine.getPhoneByUserId(nUserId));
        }
        return arrResult;
    }

    public UserInfo getById(long nId){
        UserInfo result = null;
        UserInfoDBWrapper wrapper = new UserInfoDBWrapper(getContext());
        result = wrapper.getById(nId);
        PhoneInfoEngine phoneInfoEngine = new PhoneInfoEngine(getContext());
        long nUserId = result.getId();
        result.setPhones(phoneInfoEngine.getPhoneByUserId(nUserId));
        return result;
    }

    public ArrayList<UserInfo> getBySearchString(String strSearch){
        UserInfoDBWrapper wrapper = new UserInfoDBWrapper(getContext());
        return wrapper.getBySearchString(strSearch);
    }
}
