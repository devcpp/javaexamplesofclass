package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.PhoneInfo;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.UserInfo;
import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.R;

import java.util.ArrayList;

public class NotepadExpandableAdapter extends BaseExpandableListAdapter{
    private ArrayList<UserInfo> m_arrData = null;

    private OnClickGroupListener m_listener = null;

    public NotepadExpandableAdapter(ArrayList<UserInfo> arrData){
        m_arrData = arrData;
    }

    @Override
    public int getGroupCount() {
        return m_arrData.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return ((UserInfo)getGroup(groupPosition)).getPhones().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return m_arrData.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return ((UserInfo)getGroup(groupPosition)).getPhones().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return ((UserInfo)getGroup(groupPosition)).getId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return ((PhoneInfo)getChild(groupPosition,childPosition)).getId();
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if(convertView==null){
            LayoutInflater li = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.grup_item, parent, false);
        }
        TextView tv = (TextView) convertView.findViewById(R.id.text1);
        ImageView iv = (ImageView)  convertView.findViewById(R.id.imageview1);
        UserInfo record = (UserInfo) getGroup(groupPosition);
        tv.setText(record.getFullName());

        final long nId = record.getId();
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (m_listener!=null){
                    m_listener.onClickGroup(nId);
                }
            }
        });
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if(convertView==null){
            LayoutInflater li = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(android.R.layout.simple_list_item_1, parent, false);
        }
        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
        PhoneInfo phone = (PhoneInfo)getChild(groupPosition, childPosition);
        tv.setText(phone.getPhone());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    public void setOnClickListener(OnClickGroupListener listener){
        m_listener = listener;
    }

    public interface OnClickGroupListener{
        public abstract void onClickGroup(long nId);
    }
}
