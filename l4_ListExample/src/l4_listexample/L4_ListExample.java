
package l4_listexample;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;


public class L4_ListExample {

    
    public static void main(String[] args) {
        LinkedList<Integer> arrInts = new LinkedList<Integer>();
        
        arrInts.add(23);
        arrInts.add(new Integer(15));
        int nVal = 10;
        arrInts.add(nVal);
        
        for (int i=0; i<arrInts.size(); i++){
            System.out.println("-> " + arrInts.get(i));
        }
        
        System.out.println("search -> " + arrInts.indexOf(new Integer(10)));
        System.out.println("search -> " + arrInts.indexOf(new Integer(25)));
        
        arrInts.add(2, new Integer(25));
        
        LinkedList<Integer> arrInts2 = new LinkedList<Integer>(arrInts.subList(1, 3));
        
        for (int i=0; i<arrInts2.size(); i++){
            System.out.println("-> " + arrInts2.get(i));
        }
    }
    
}
