package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model.Wrappers.NetworkWrappers;

import android.content.Context;

public class BaseNetworkWrapper {

    public static final String SERVER_NAME = "http://xutpuk.pp.ua/api/";

    public static final int SERVER_RESPONSE_OK = 200;

    private Context m_context = null;

    public BaseNetworkWrapper(Context context){
        m_context = context;
    }

    public Context getContext(){
        return m_context;
    }
}
