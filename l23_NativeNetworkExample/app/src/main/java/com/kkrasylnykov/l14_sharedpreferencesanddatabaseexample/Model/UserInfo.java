package com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.Model;

import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l14_sharedpreferencesanddatabaseexample.ToolsAndConstants.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UserInfo {
    private long m_nId = -1;
    private long m_nServerId = -1;
    private String m_strName = "";
    private String m_strSName = "";
    private ArrayList<PhoneInfo> m_arrPhones = new ArrayList<>();
    private String m_strAddress = "";
    private int m_nBYear = -1;

    private String m_strFullName = "";

    public UserInfo(Cursor cursor){
        setId(cursor.getLong(0));
        setServerId(cursor.getLong(1));
        setName(cursor.getString(2));
        setSName(cursor.getString(3));
        setAddress(cursor.getString(4));
        setBYear(cursor.getInt(5));
    }

    public UserInfo(JSONObject jsonObject){
        setId(-1);
        try {
            setServerId(jsonObject.getLong(AppConstants.JSON_USERS.FIELD_SERVER_ID));
            setName(jsonObject.getString(AppConstants.JSON_USERS.FIELD_NAME));
            setSName(jsonObject.getString(AppConstants.JSON_USERS.FIELD_SNAME));
            setPhones(jsonArrayTOPhoneInfo(jsonObject.getJSONArray(AppConstants.JSON_USERS.FIELD_PHONES)));
            setAddress(jsonArrayToAddress(jsonObject.getJSONArray(AppConstants.JSON_USERS.FIELD_ADDRESS)));
            setBYear(-1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public UserInfo(String strName, String strSName, ArrayList<PhoneInfo> arrPhones, String strAddress, int nBYear){
        setServerId(-1);
        setName(strName);
        setSName(strSName);
        setPhones(arrPhones);
        setAddress(strAddress);
        setBYear(nBYear);
    }

    public long getId() {
        return m_nId;
    }

    public void setId(long nId) {
        m_nId = nId;
    }

    public long getServerId() {
        return m_nServerId;
    }

    public void setServerId(long nServerId) {
        this.m_nServerId = nServerId;
    }

    public String getName() {
        return m_strName;
    }

    public void setName(String m_strName) {
        this.m_strName = m_strName;
    }

    public String getSName() {
        return m_strSName;
    }

    public void setSName(String m_strSName) {
        this.m_strSName = m_strSName;
    }

    public ArrayList<PhoneInfo> getPhones() {
        return m_arrPhones;
    }

    public void setPhones(ArrayList<PhoneInfo> arrPhones) {
        this.m_arrPhones = arrPhones;
    }

    public String getAddress() {
        return m_strAddress;
    }

    public void setAddress(String m_strAddress) {
        this.m_strAddress = m_strAddress;
    }

    public int getBYear() {
        return m_nBYear;
    }

    public void setBYear(int m_nBYear) {
        this.m_nBYear = m_nBYear;
    }

    public String getFullName() {
        return getName() + " " + getSName();
    }

    private String jsonArrayToAddress(JSONArray jsonArray){
        String strResult = "";
        int nCount = jsonArray.length();
        try {
            if(nCount>0){
                strResult = jsonArray.getString(0);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return strResult;
    }

    private ArrayList<PhoneInfo> jsonArrayTOPhoneInfo(JSONArray jsonArray){
        ArrayList<PhoneInfo> arrResult = new ArrayList<>();
        int nCount = jsonArray.length();
        try {
            for (int i=0; i<nCount; i++){
                String strPhone = jsonArray.getString(i);
                arrResult.add(new PhoneInfo(-1,-1,strPhone));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arrResult;
    }

    private JSONArray phonesToJSONArray(){
        JSONArray arrReturn = new JSONArray();
        ArrayList<PhoneInfo> arrPhones = getPhones();
        for (PhoneInfo info:arrPhones){
            arrReturn.put(info.getPhone());
        }
        return arrReturn;
    }

    private JSONArray addressToJSONArray(){
        JSONArray arrReturn = new JSONArray();
        arrReturn.put(getAddress());
        return arrReturn;
    }

    public JSONObject getJSONObject(){
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put(AppConstants.JSON_USERS.FIELD_NAME, getName());
            jsonObj.put(AppConstants.JSON_USERS.FIELD_SNAME, getSName());
            jsonObj.put(AppConstants.JSON_USERS.FIELD_PHONES, phonesToJSONArray());
            jsonObj.put(AppConstants.JSON_USERS.FIELD_ADDRESS, addressToJSONArray());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonObj = null;
        }
        return jsonObj;
    }

    public ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        values.put(AppConstants.DB_TABLE_USERS.FIELD_SERVER_ID, getServerId());
        values.put(AppConstants.DB_TABLE_USERS.FIELD_NAME, getName());
        values.put(AppConstants.DB_TABLE_USERS.FIELD_SNAME,getSName());
        values.put(AppConstants.DB_TABLE_USERS.FIELD_ADDRESS,getAddress());
        values.put(AppConstants.DB_TABLE_USERS.FIELD_BYEAR,getBYear());
        return values;
    }
}
