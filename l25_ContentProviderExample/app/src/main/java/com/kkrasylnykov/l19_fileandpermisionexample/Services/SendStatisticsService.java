package com.kkrasylnykov.l19_fileandpermisionexample.Services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.kkrasylnykov.l19_fileandpermisionexample.Acvtivites.MainActivity;
import com.kkrasylnykov.l19_fileandpermisionexample.Model.FileOnServer;
import com.kkrasylnykov.l19_fileandpermisionexample.Model.Wrappers.ContentProviderWrappers.SettingsContentProviderWrapper;
import com.kkrasylnykov.l19_fileandpermisionexample.Tools.Settings;

import java.util.ArrayList;

public class SendStatisticsService extends Service {

    Thread m_thread = null;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("devcppl19Service","onStartCommand ->");
        final ArrayList<FileOnServer> arrData = MainActivity.getFiles();
        if (m_thread ==null){
            m_thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true){
                        String strDataForOutput = "";
                        for (FileOnServer file:arrData){
                            strDataForOutput += file.getLocalName() + " -> " + SettingsContentProviderWrapper.getCountShowImage(SendStatisticsService.this,file.getLocalName())+"; ";
                        }
                        Log.d("devcppl19Service","Data -> " + strDataForOutput);
                        try {
                            Thread.sleep(20000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            m_thread.start();

        }
        return 0;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
