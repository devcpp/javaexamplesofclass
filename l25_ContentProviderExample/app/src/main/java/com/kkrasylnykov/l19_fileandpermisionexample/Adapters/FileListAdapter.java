package com.kkrasylnykov.l19_fileandpermisionexample.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kkrasylnykov.l19_fileandpermisionexample.Model.FileOnServer;

import java.io.File;
import java.util.ArrayList;

public class FileListAdapter extends BaseAdapter {

    ArrayList<FileOnServer> m_arrData = null;

    public FileListAdapter(ArrayList<FileOnServer> arrData){
        m_arrData = arrData;
    }

    @Override
    public int getCount() {
        return m_arrData.size();
    }

    @Override
    public Object getItem(int i) {
        return m_arrData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view==null){
            LayoutInflater li = (LayoutInflater)viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(android.R.layout.simple_list_item_1, viewGroup, false);
        }
        TextView tv = (TextView) view.findViewById(android.R.id.text1);
        tv.setText(((FileOnServer)getItem(i)).getLocalName());
        return view;
    }
}
