package com.kkrasylnykov.l19_fileandpermisionexample.Model.Wrappers.ContentProviderWrappers;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.kkrasylnykov.l19_fileandpermisionexample.ContentProviders.SettingsContentProvider;

public class SettingsContentProviderWrapper {

    public static int getCountShowImage(Context context, String strImage){
        int nResult = -1;
        Cursor cursor = null;
        Uri uri = Uri.parse(SettingsContentProvider.COUNT_SHOW_FILE_URI.toString()+"/"+strImage);
        cursor = context.getContentResolver().query(uri, null, null, null, null);
        if(cursor != null && cursor.moveToFirst()){
            nResult = cursor.getInt(1);
        }
        if(cursor != null){
            cursor.close();
        }
        return nResult;
    }

    public static void setCountShowImage(Context context, String strImage, int nCount){
        Uri uri = Uri.parse(SettingsContentProvider.COUNT_SHOW_FILE_URI.toString()+"/"+strImage);
        ContentValues contentValues = new ContentValues();
        contentValues.put(SettingsContentProvider.DATA, nCount);
        context.getContentResolver().insert(uri, contentValues);
    }
}
