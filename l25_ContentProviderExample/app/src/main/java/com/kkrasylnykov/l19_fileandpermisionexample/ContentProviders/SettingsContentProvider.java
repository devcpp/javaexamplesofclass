package com.kkrasylnykov.l19_fileandpermisionexample.ContentProviders;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import com.kkrasylnykov.l19_fileandpermisionexample.Acvtivites.MainActivity;
import com.kkrasylnykov.l19_fileandpermisionexample.Model.FileOnServer;
import com.kkrasylnykov.l19_fileandpermisionexample.Tools.Settings;

import java.util.ArrayList;

public class SettingsContentProvider extends ContentProvider {

    private static final String AUTHORITY = "com.kkrasylnykov.l19_fileandpermisionexample.ContentProviders.SettingsContentProvider";

    public static final String DATA = "DATA";

    public static final String COUNT_SHOW_FILE     = "count_show_file";
    public static final String COUNT_SHOW_FILES     = "count_show_files";

    public static final Uri COUNT_SHOW_FILE_URI    = Uri.parse("content://" + AUTHORITY + "/" + COUNT_SHOW_FILE);
    public static final Uri COUNT_SHOW_FILES_URI    = Uri.parse("content://" + AUTHORITY + "/" + COUNT_SHOW_FILES);

    public static final int URI_COUNT_SHOW_FILE = 1;
    public static final int URI_COUNT_SHOW_FILES = 2;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, COUNT_SHOW_FILE + "/*", URI_COUNT_SHOW_FILE);
        uriMatcher.addURI(AUTHORITY, COUNT_SHOW_FILES, URI_COUNT_SHOW_FILES);


    }

    @Override
    public boolean onCreate() {
        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] strings, String s, String[] strings1, String s1) {
        String[] arrColumnNames = {"file_name", "count"};
        MatrixCursor result = new MatrixCursor(arrColumnNames);
        switch (uriMatcher.match(uri)){
            case URI_COUNT_SHOW_FILE:{
                String strFileId = uri.getLastPathSegment();
                int nCount = Settings.getCountShowImage(getContext(), strFileId);
                Object[] data = new Object[]{strFileId,new Integer(nCount)};
                result.addRow(data);
            }
            break;
            case URI_COUNT_SHOW_FILES:
            {
                ArrayList<FileOnServer> arrData = MainActivity.getFiles();
                for (FileOnServer file:arrData){
                    String strFileId = file.getLocalName();
                    int nCount = Settings.getCountShowImage(getContext(), strFileId);
                    Object[] data = new Object[]{strFileId,new Integer(nCount)};
                    result.addRow(data);
                }
            }
                break;
            default:
                Log.e("devcppl19", "query - error! -> " + uri.toString());
        }

        return result;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        switch (uriMatcher.match(uri)) {
            case URI_COUNT_SHOW_FILE: {
                String strFileId = uri.getLastPathSegment();
                int nCount = contentValues.getAsInteger(DATA);
                Settings.setCountShowImage(getContext(), strFileId, nCount);
            }
            break;
        }
        return uri;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }



    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }
}
